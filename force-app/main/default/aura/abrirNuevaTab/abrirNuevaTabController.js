/**
 * @description       : 
 * @last modified on  : 22-11-2021
 * @last modified by  : Alejandro Hernández
**/
({
    openTab : function(component, event, helper) {
        console.log("@@@@@ AURA @@@@@");
        console.log(event.getParam("caseId"))
        console.log(event)
        var workspaceAPI = component.find("workspace");
        workspaceAPI.openTab({
            pageReference: {
                "type": "standard__recordPage",
                "attributes": {
                    "recordId": event.getParam("caseId"),
                    "actionName":"view"
                },
                "state": {}
            },
            focus: true
        }).then(function(response) {
            workspaceAPI.getTabInfo({
                tabId: response
        }).then(function(tabInfo) {
            console.log("The recordId for this tab is: " + tabInfo.recordId);
        });
        }).catch(function(error) {
            console.log(error);
        });
    },
})
