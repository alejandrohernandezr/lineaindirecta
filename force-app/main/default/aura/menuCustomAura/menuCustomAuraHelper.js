/**
 * @description       : 
 * @last modified on  : 24-11-2021
 * @last modified by  : Alejandro Hernández
**/
({
    doInit: function (component, event) {
      let actionGetMenuItems = component.get("c.getMenuItems");
      actionGetMenuItems.setCallback(this, function (response) {
        if (response.getState() === "SUCCESS") {
          component.set("v.listMenu", response.getReturnValue());
        }
      });
      $A.enqueueAction(actionGetMenuItems);
  
      let actionGetInfoUser = component.get("c.getInfoUser");
      actionGetInfoUser.setCallback(this, function (response) {
        if (response.getState() === "SUCCESS") {
          component.set("v.infoClient", JSON.parse(response.getReturnValue())[0]);
          let info = JSON.parse(response.getReturnValue())[0];
          if(info && info.Contact && info.Contact.Account && info.Contact.Account.NivelViajeroAplicado__c) {
            component.set('v.nivel',info.Contact.Account.NivelViajeroAplicado__r.Name)
            let userObj = { 'nivel' : info.Contact.Account.NivelViajeroAplicado__r.Name }
            localStorage.setItem('ham',JSON.stringify(userObj))
          }
        }
      });
      $A.enqueueAction(actionGetInfoUser);
    }, 
    navigateTo: function (component, event) {
      let navServ = component.find("navService");
      let url = event.currentTarget.dataset.url;
      let pageDest;
  
      if (url === "profile") {
        pageDest = {
          type: "standard__recordPage",
          attributes: {
            objectApiName: 'profile',
            recordId: $A.get("$SObjectType.CurrentUser.Id"),
            actionName: "view"
          }
        };
      } else {
        pageDest = {
          type: !eval(event.currentTarget.dataset.external) ? 'comm__namedPage' : 'standard__webPage',
          attributes: {
              pageName: url,
              url: url
          }
        };
      }
    
      navServ.navigate(pageDest, true);
      component.set("v.showMainMenu", false)
  
      // var closeIcons = component.get('c.closeIcon')
      $A.enqueueAction(component.get('c.closeIcon'));
      component.set("v.showUserMenu", false);
  
      let menuCloseBack = document.querySelectorAll('.close-div');
      menuCloseBack[0].style.display = "none";
  
    },
    clickUserMenu: function (component, event) {
      let menuCloseBack = document.querySelectorAll('.close-div');
      menuCloseBack[0].style.display = "block";
      component.set("v.showUserMenu", !component.get("v.showUserMenu"));
      $A.enqueueAction(component.get('c.closeIcon'));
    },
    clickMainMenu: function (component, event) {
      let menuCloseBack = document.querySelectorAll('.close-div');
      menuCloseBack[0].style.display = "block";
      component.set("v.showMainMenu", !component.get("v.showMainMenu"));
    },
    commLogout: function (component, event) {
      let actionLogout = component.get("c.getCommunityUrlLogout");
  
      actionLogout.setCallback(this, function (response) {
        if (response.getState() === "SUCCESS") {
          let url = response.getReturnValue();
          window.location.replace(
            url + "/secur/logout.jsp?retUrl=" + url.replace("/", "%2F")
          );
        }
      });
      $A.enqueueAction(actionLogout);
    }, 
  });