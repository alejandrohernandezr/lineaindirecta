/**
 * @description       : 
 * @last modified on  : 24-11-2021
 * @last modified by  : Alejandro Hernández
**/
({
    doInit: function(component, event, helper) {
        helper.doInit(component, event)
    },
    navigateTo: function(component, event, helper) {
        helper.navigateTo(component, event)
    },
    clickUserMenu: function(component, event, helper) {
        helper.clickUserMenu(component, event)
    },
    clickMainMenu: function(component, event, helper) {
        helper.clickMainMenu(component, event)
    },
    commLogout: function(component, event, helper) {
        helper.commLogout(component, event)
    },
    toggleIconClass: function(component, event, helper) {
        helper.toggleIconClass(component,event)
    },
    toggleIconClass: function(component, event, helper) {
        var toggleText = component.find("menu-icon");
        var toggleTextDesktop = component.find("menu-icon-desktop");
        $A.util.toggleClass(toggleText, "open");
        $A.util.toggleClass(toggleTextDesktop, "open");
        helper.clickMainMenu(component, event);
        component.set("v.showUserMenu", false);
    },
    closeIcon: function(component, event) {
        var toggleText2 = component.find("menu-icon");
        var toggleTextDesktop2 = component.find("menu-icon-desktop");
        $A.util.removeClass(toggleText2, "open");
        $A.util.removeClass(toggleTextDesktop2, "open");
        component.set("v.showMainMenu", false);
    },

})