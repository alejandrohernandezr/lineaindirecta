/**
 * @description       : 
 * @last modified on  : 22-11-2021
 * @last modified by  : Alejandro Hernández
**/
public with sharing class CaseTriggerHandler {

    public static void asignarCasoAQuienMenosCasosTenga(Case caso, Map<String, Integer> numCasosPorAgente) {
        Integer minimo = 100000;
        String idAgente = '';

        for (String agente : numCasosPorAgente.keySet()) {
            if (numCasosPorAgente.get(agente) < minimo) {
                idAgente = agente;
                minimo = numCasosPorAgente.get(agente);
            }
        }

        caso.OwnerId = idAgente;
        numCasosPorAgente.put(idAgente, minimo+1);
    }
}
