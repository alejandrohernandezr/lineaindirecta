/**
 * @description       : 
 * @author            : David LÃ³pez <sdlopez@omegacrmconsulting.com>
 * @test class        : 
 * @last modified on  : 24-11-2021
 * @last modified by  : Alejandro Hernández
 * Modifications Log 
 * Ver   Date         Author                                         Modification
 * 1.0   03-10-2021   David LÃ³pez <sdlopez@omegacrmconsulting.com>   Initial Version
**/
public without sharing class CH_MenuController {
    
    @AuraEnabled
    public static List<HAM_Comm_Helysia_Menu__mdt> getMenuItems(){
        return [SELECT MasterLabel, HAM_Index__c, HAM_URL__c, HAM_External__c FROM HAM_Comm_Helysia_Menu__mdt ORDER BY HAM_Index__c ASC];
    }

    @AuraEnabled
    public static String getCommunityUrlLogout(){
        try{
            return (ConnectApi.Communities.getCommunity(Network.getNetworkId())).siteUrl;
        } catch(Exception ex) {
            return 'No se puede obtener url';
        }
    }

    @AuraEnabled
    public static String getInfoUser(){        
        return JSON.serialize([SELECT Id, FullPhotoUrl, Contact.Name, Contact.Account.NivelViajeroAplicado__r.Name FROM User WHERE Id = :UserInfo.getUserId()]);
    }
}