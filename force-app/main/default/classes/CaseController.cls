/**
 * @description       : 
 * @last modified on  : 22-11-2021
 * @last modified by  : Alejandro Hernández
**/
public with sharing class CaseController {
    private static final Integer FALTAN_CAMPOS = 0;
    private static final Integer OK = 1;
    private static final Integer ERROR = 2;

    @AuraEnabled
    public static List<Case> getCasesFromUser() {
        try {
            id userId = UserInfo.getUserId();
            User u = [select id, contactId from User where id = : userId];
            id getContactId = u.contactId;
            system.debug('getContactId' + getContactId);

            return [SELECT Id, Subject, Status, Description, Type, Poliza__r.Name FROM Case WHERE ContactId = :getContactId];
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static List<Poliza__c> getPolizasFromUser() {
        try {
            id userId = UserInfo.getUserId();
            User u = [select id, contactId from User where id = : userId];
            id getContactId = u.contactId;
            system.debug('getContactId' + getContactId);

            return [SELECT Id, Name FROM Poliza__c WHERE Contact__c = :getContactId];
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static Integer crearCase(Case caseFromWebsite, String poliza) {
        try {
            Case newCase = new Case();
            Integer result = ERROR;
            id userId = UserInfo.getUserId();
            User u = [select id, contactId from User where id = : userId];
            id getContactId = u.contactId;

            // if (String.isBlank(caseFromWebsite.Subject) || String.isBlank(caseFromWebsite.Type)) {
            //     result = FALTAN_CAMPOS;
            // } else {
                newCase.ContactId = u.contactId;
                newCase.Subject = caseFromWebsite.Subject;
                newCase.Type = caseFromWebsite.Type;
                newCase.Origin = 'Web';
                newCase.Poliza__c = poliza;
                newCase.Description = caseFromWebsite.Description;

                insert newCase;

                result = OK;
            // }

            return result;

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static string getCamposFormulario(String tipoForm) {
        try {
            return JSON.serialize(OM_SchemaUtils.getFieldSet('Case', tipoForm));
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}
