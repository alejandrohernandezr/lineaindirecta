/**
 * @description       : 
 * @last modified on  : 18-11-2021
 * @last modified by  : Alejandro Hernández
**/
public with sharing class ControladorRegistro {
    public static final String OK = 'OK';

    @AuraEnabled
    public static string registrar(Contact accountFromWebsite, String numPoliza) {
        try {
            String checkResult = '';
            List<Contact> emailAlreadyRegistered = [SELECT Id, Email FROM Contact WHERE Email = :accountFromWebsite.Email LIMIT 1];
            List<Contact> DNIAlreadyRegistered = [SELECT Id FROM Contact WHERE DNI__c = :accountFromWebsite.DNI__c LIMIT 1];
            List<Poliza__c> numeroPolizaAlreadyRegistered = [SELECT Id FROM Poliza__c WHERE Name =:numPoliza LIMIT 1];

            checkResult = controladorRegistro.checkFieldsBeforeInsert(accountFromWebsite, emailAlreadyRegistered, DNIAlreadyRegistered, numeroPolizaAlreadyRegistered);

            if (checkResult == OK) {
                Account acc = new Account();
                acc.Name = 'Community User ' + accountFromWebsite.FirstName + ' ' + accountFromWebsite.LastName;
                insert acc;

                Contact contact = new Contact();
                contact.AccountId = acc.Id;
                contact.FirstName = accountFromWebsite.FirstName;
                contact.LastName = accountFromWebsite.LastName;
                contact.Email = accountFromWebsite.Email;
                contact.DNI__c = accountFromWebsite.DNI__c;

                insert contact;

                if (numPoliza != null) {
                    Poliza__c poliza = new Poliza__c();
                    poliza.Name = numPoliza;
                    poliza.Contact__c = contact.Id;
                    insert poliza;
                }

                Profile profile = [SELECT Id  FROM Profile WHERE Name = 'Customer Community Login User Custom' LIMIT 1];

                User user = new User();
                user.ContactId = contact.Id;
                user.Username = contact.Email;
                user.put('Email', contact.Email);
                user.FirstName = contact.FirstName;
                user.LastName = contact.LastName;
                user.LanguageLocaleKey = 'es';
                user.TimeZoneSidKey = 'Europe/Paris';
                user.EmailEncodingKey = 'UTF-8';
                user.CommunityNickname = randomStr(2) + contact.Email.substringBefore('@');
                user.Alias = contact.LastName.left(8);
                user.LocaleSidKey = 'es';
                user.ProfileId = profile.Id;
                user.IsActive = true;
    
                insert user;
            } 

            return checkResult;

            

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    private static String checkFieldsBeforeInsert(Contact acc, List<Contact> emailAlreadyRegistered, List<Contact> DNIAlreadyRegistered, List<Poliza__c> numeroPolizaAlreadyRegistered) {
        String returnMessage = OK;

        if (checkIfAllFieldsAreFilled(acc)) {
            returnMessage = 'Asegúrese de haber rellenado todos los campos';
        } else if (emailAlreadyRegistered != null && !emailAlreadyRegistered.isEmpty()) {
            returnMessage = 'Ya existe un usuario con ese email';
        } else if (DNIAlreadyRegistered != null && !DNIAlreadyRegistered.isEmpty()) {
            returnMessage = 'Ya existe un usuario con ese DNI';
        } else if (numeroPolizaAlreadyRegistered != null && !numeroPolizaAlreadyRegistered.isEmpty()) {
            returnMessage = 'Ya existe esa póliza en el sistema';
        }

        return returnMessage;
    }

    private static Boolean checkIfAllFieldsAreFilled(Contact acc) {
        return (String.isBlank(acc.FirstName) || String.isBlank(acc.LastName) || String.isBlank(acc.DNI__c) || String.isBlank(acc.Email));
    }

    private static String randomStr(Integer len) {
        final String chars = '123456789';
        String randStr = '';

        while (randStr.length() < len) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
            randStr += chars.substring(idx, idx+1);
        }
        return randStr; 
    }
}
