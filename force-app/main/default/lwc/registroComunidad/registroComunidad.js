/**
 * @description       : 
 * @last modified on  : 18-11-2021
 * @last modified by  : Alejandro Hernández
**/
import { LightningElement, track } from 'lwc';
import registrar from '@salesforce/apex/ControladorRegistro.registrar'

export default class RegistroComunidad extends LightningElement {
    @track camposFormulario = []
    @track usuario = { sObjectType: 'Contact' }

    connectedCallback() {
        this.generarCamposFormulario()
    }

    actualizarValorJs(event) {
        this.usuario[event.target.dataset.input] = event.target.value
    }

    registroJs() {
        registrar({ accountFromWebsite : this.usuario, numPoliza : this.usuario['NumeroPoliza__c'] })
        .then( res => {
            console.log(res)
        })
        .catch( error => console.error(error))
    }

    generarCamposFormulario() {
        this.camposFormulario = [
            { 'label': 'Nombre', 'datainput': 'FirstName', 'required': true },
            { 'label': 'Apellidos', 'datainput': 'LastName', 'required': true },
            { 'label': 'DNI', 'datainput': 'DNI__c', 'required': true },
            { 'label': 'Email', 'datainput': 'Email', 'required': true },
            { 'label': 'Número de poliza (si lo conoce)', 'datainput': 'NumeroPoliza__c', 'required': false },
            { 'label': 'Calle', 'datainput': 'BillingStreet__c', 'required': false },
            { 'label': 'Ciudad', 'datainput': 'BillingCity__c', 'required': false },
            { 'label': 'Provincia', 'datainput': 'BillingState__c', 'required': false },
            { 'label': 'Código Postal', 'datainput': 'BillingPostalCode__c', 'required': false }
        ]
    }
}