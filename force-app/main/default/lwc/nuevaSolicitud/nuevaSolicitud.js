/**
 * @description       : 
 * @last modified on  : 22-11-2021
 * @last modified by  : Alejandro Hernández
**/
import { LightningElement, track } from 'lwc';
import getPolizasFromUser from '@salesforce/apex/CaseController.getPolizasFromUser'
import crearCase from '@salesforce/apex/CaseController.crearCase'
import getCamposFormulario from '@salesforce/apex/CaseController.getCamposFormulario'
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class NuevaSolicitud extends LightningElement {
    FALTAN_CAMPOS = 0
    OK = 1
    ERROR = 2

    @track objectApiName = 'Case'
    @track fields = []
    // @track fields = ['Subject', 'Description', 'Type']
    @track numeroPoliza
    @track polizas = []
    @track caso = { sObjectType : 'Case' }
    @track seleccionadoTipoCaso = false

    get options() {
        return [
            { label : 'Solicitud de grúa', value : 'SolicitudGrua' },
            { label : 'Renovación de seguro', value : 'RenovacionSeguro' },
            { label : 'Nuevo seguro', value : 'NuevoSeguro' }
        ]
    }

    connectedCallback() {
        this.getPolizasFromUserJs()
    }

    crearCaseJs() {
        crearCase({ caseFromWebsite : this.caso, poliza : this.numeroPoliza })
        .then( res => {
            if (res === this.FALTAN_CAMPOS) {
                this.lanzarToast('Faltan campos', 'Faltan uno o más campos, revíselos por favor', 'warning')
            } else if (res === this.OK) {
                this.lanzarToast('OK!', 'Se ha creado la nueva incidencia', 'success')
            }
        }).catch( err => { console.error(err) })
    }

    actualizarNumPolizaJs(event) {
        this.numeroPoliza = event.detail.value
    }

    actualizarFormularioJs(event) {
        getCamposFormulario({ tipoForm : event.detail.value })
        .then( res => {
            if (res != null) {
                console.log(JSON.parse(res))
                this.fields = JSON.parse(res)
                this.seleccionadoTipoCaso = true
            }
        }).catch(err => { console.error(err) })
    }

    actualizarCaso(event) {
        this.caso[event.target.dataset.input] = event.target.value
    }

    getPolizasFromUserJs() {
        getPolizasFromUser()
        .then( res => {
            this.polizas = res.map( val => { return { label: val.Name, value: val.Id } })
        }).catch( err => { console.error(err) })
    }

    lanzarToast(title, message, variant, mode='dismissible') {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
            mode: mode
        })
        this.dispatchEvent(evt);
    }
}