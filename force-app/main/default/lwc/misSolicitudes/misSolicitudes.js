/**
 * @description       : 
 * @last modified on  : 19-11-2021
 * @last modified by  : Alejandro Hernández
**/
import { LightningElement, track } from 'lwc';
import getCasesFromUser from '@salesforce/apex/CaseController.getCasesFromUser';

export default class MisSolicitudes extends LightningElement {
    @track hayCasos = false
    @track casos = []
    @track columnas = [
        { fieldName : "Subject", label : "Asunto" },
        { fieldName : "Poliza__r.Name", label : "Póliza" },
        { fieldName : "Description", label : "Descripción" },
        { fieldName : "Type", label : "Tipo" }
    ]

    connectedCallback() {
        this.getCasesFromUserJs()
    }

    getCasesFromUserJs() {
        getCasesFromUser()
        .then( res => {
            if (res != null) {
                this.casos = res;
                this.hayCasos = true;
            }
        }).catch( err => console.error(err))
    }
}