/**
 * @description       : 
 * @last modified on  : 22-11-2021
 * @last modified by  : Alejandro Hernández
**/
import { LightningElement } from 'lwc';
import { subscribe, unsubscribe, onError, setDebugFlag, isEmpEnabled } from 'lightning/empApi';

export default class AbrirNuevaTabLwc extends LightningElement {
    channelName = '/data/CaseChangeEvent'
    subscription = {}
    caseId 

    connectedCallback() {
        this.registerErrorListener()
        this.handleSubscribe()
    }

    
    handleSubscribe() {
        // Callback invoked whenever a new event message is received
        const messageCallback = (response) => {
            // TODO: lanzar evento al padre
            this.dispatchEvent(new CustomEvent('abrirtab', { detail: { caseId: response.data.payload.ChangeEventHeader.recordIds[0] }}))
            // this.template.querySelector("miTabla").refreshData()
            console.log('New message received: ', JSON.stringify(response));
            // Response contains the payload of the new message received
        };

        // Invoke subscribe method of empApi. Pass reference to messageCallback
        subscribe(this.channelName, -1, messageCallback).then((response) => {
            // Response contains the subscription information on subscribe call
            console.log(
                'Subscription request sent to: ',
                JSON.stringify(response.channel)
            );

            this.subscription = response;
        });
    }

    registerErrorListener() {
        // Invoke onError empApi method
        onError((error) => {
            console.log('Received error from server: ', JSON.stringify(error));
            // Error contains the server-side error
        });
    }
}