/**
 * @description       : 
 * @last modified on  : 23-11-2021
 * @last modified by  : Alejandro Hernández
**/
import { LightningElement, track } from 'lwc';
import { CloseActionScreenEvent } from 'lightning/actions';
import getCasesFromUser from '@salesforce/apex/CaseController.getCasesFromUser';
 
export default class LwcQuickAction extends LightningElement {
    @track casos = []
    @track columnas = [
        { fieldName : "Subject", label : "Asunto" },
        { fieldName : "Poliza__r.Name", label : "Póliza" },
        { fieldName : "Description", label : "Descripción" },
        { fieldName : "Type", label : "Tipo" }
    ]

    connectedCallback() {
        this.getCasesFromUserJs()
    }

    getCasesFromUserJs() {
        getCasesFromUser()
        .then( res => {
            if (res != null) {
                this.casos = res;
            }
        }).catch( err => console.error(err))
    }

    closeAction() {
        this.dispatchEvent(new CloseActionScreenEvent());
    }
}