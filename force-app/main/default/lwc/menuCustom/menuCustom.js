/**
 * @description       : 
 * @last modified on  : 24-11-2021
 * @last modified by  : Alejandro Hernández
**/
import { LightningElement, track, api, wire } from 'lwc';
import communityId from '@salesforce/community/Id';
import communityBasePath from '@salesforce/community/basePath';
import getNavItems from '@salesforce/apex/NavMenuController.getConnectNavigationItems';
import {NavigationMixin} from "lightning/navigation";


export default class MenuCustom extends LightningElement {
    @wire(getNavItems, {
        menuName: '$menuName',
        communityId: '$communityId'
      })
      wiredNavigationItems({ error, data }) {
        if (data) {
          this.menuItems = data.menuItems;
        } else if (error) {
          this.error = error;
        }
      }

      navigateToItem(event) {
        // Get the menu item's label from the target
        let selectedLabel = event.currentTarget.dataset.label;
        let item = {};
    
        // Filter the menu items for the selected item
        item = this.menuItems.filter(
          menuItem => menuItem.label === selectedLabel
        )[0];
        
        // Distribute the action to the relevant mechanisim for navigation
        if (item.urlType === "ExternalLink") {
          this.navigateToExternalPage(item);
        } else if (item.urlType === "InternalLink") {
          this.navigateToInternalPage(this.baseUrl, item);
        }
      }

        // Navigate to an external link
  navigateToExternalPage(item) {
    const url = item.url;
    if (item.target === "CurrentWindow") {
      window.open(url, "_self");
    } else if (item.target === "NewWindow") {
      this[NavigationMixin.Navigate]({
        type: "standard__webPage",
        attributes: {
        url: url
        }
      });
    }
  }
    
}