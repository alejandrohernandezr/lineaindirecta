/**
 * @description       : 
 * @last modified on  : 22-11-2021
 * @last modified by  : Alejandro Hernández
**/
trigger CaseTrigger on Case (before delete, before insert, before update, after delete, after insert, after update) {
 
    Map<String, Integer> numCasosPorAgente = new Map<String, Integer>();
    Map<Id, Case> casos;
    List<User> agentes;
    /*
     _         _ _     _          __            
    | |__ _  _| | |__ | |__  ___ / _|___ _ _ ___
    | '_ \ || | | / / | '_ \/ -_)  _/ _ \ '_/ -_)
    |_.__/\_,_|_|_\_\ |_.__/\___|_| \___/_| \___| 
    */       
    if(Trigger.isBefore) {
        if(Trigger.isInsert) {
            agentes = [SELECT Id FROM User WHERE Profile.Name = 'Partner App Subscription User'];

            for (Case caso : [SELECT Id, OwnerId FROM Case]) {
                Integer numCasos = numCasosPorAgente.containsKey(caso.OwnerId) ? numCasosPorAgente.get(caso.OwnerId)+1 : 0;
                numCasos = numCasosPorAgente.put(caso.OwnerId, numCasos);
            }

            for (User user : agentes) {
                if (!numCasosPorAgente.containsKey(user.Id)) {
                    numCasosPorAgente.put(user.Id, 0);
                }
            }

            System.debug('@@@@@@@@@@@@@@@@@@ CASOS:'+JSON.serializePretty(numCasosPorAgente));
        }
        if(Trigger.isUpdate) {}
    }

    if(Trigger.isAfter) {
        if(Trigger.isInsert) {

        }
        if(Trigger.isUpdate) {}
    }
         

    /*                         _           _   _            
         _ _ ___ __ ___ _ _ __| |  __ _ __| |_(_)___ _ _  ___
        | '_/ -_) _/ _ \ '_/ _` | / _` / _|  _| / _ \ ' \(_-<
        |_| \___\__\___/_| \__,_| \__,_\__|\__|_\___/_||_/__/
    */
    for(Case caso : (List<Case>) Trigger.new) {
        if(Trigger.isBefore) {
            if(Trigger.isInsert) {
                CaseTriggerHandler.asignarCasoAQuienMenosCasosTenga(caso, numCasosPorAgente);
            }
            
            if(Trigger.isUpdate) {}
        }

        if(Trigger.isAfter) {
            if(Trigger.isInsert) {
                
            }
            if(Trigger.isUpdate) {}
        }
    }


    /*
         _         _ _           __ _          
        | |__ _  _| | |__  __ _ / _| |_ ___ _ _
        | '_ \ || | | / / / _` |  _|  _/ -_) '_|
        |_.__/\_,_|_|_\_\ \__,_|_|  \__\___|_| 
    */
     
    if(Trigger.isBefore) {
        if(Trigger.isInsert) {
        }
        if(Trigger.isUpdate) {}
    }

    if(Trigger.isAfter) {
        if(Trigger.isInsert) {}
        if(Trigger.isUpdate) {}
    }
}